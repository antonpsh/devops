import os
import requests
import subprocess
from datetime import datetime

def download_file(url, filename):
    response = requests.get(url)
    if response.status_code == 200:
        with open(filename, "wb") as f:
            f.write(response.content)

def convert_file(input_file, output_file):
    subprocess.run(["/usr/local/bin/python3", "/app/convert.py", input_file, output_file], check=True)

def main():
    os.chdir("/app/")
    
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    url = f"https://gitlab.com/antonpsh/devops/-/raw/main/README.md?ref_type=heads&timestamp={timestamp}"

    input_file = "index.md"
    output_file = "index.html"

    download_file(url, input_file)

    convert_file(input_file, output_file)

if __name__ == "__main__":
    main()
