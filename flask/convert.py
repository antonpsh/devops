import sys
import os
import markdown

def convert_markdown_to_html(input_file, output_file):
    # read file
    with open(input_file, 'r') as f:
        text = f.read()
        html = markdown.markdown(text, output_format='html')

    # make dir templates/ if not exist
    os.makedirs(os.path.dirname(output_file), exist_ok=True)

    # write file
    with open(output_file, 'w') as f:
        f.write(html)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python convert.py <input_file> <output_file>")
        sys.exit(1)
    
    input_file = sys.argv[1]
    output_file = os.path.join('templates', sys.argv[2])
    convert_markdown_to_html(input_file, output_file)