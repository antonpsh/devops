from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')

@app.route('/hello')
def hello():
    return render_template('hello.html', 
                           user_agent=request.headers.get('User-Agent'), 
                           remote_addr=request.remote_addr, 
                           accept_languages=request.accept_languages, 
                           referrer=request.referrer, 
                           method=request.method, 
                           url=request.url)



if __name__ == "__main__":
    app.run()