# tests/test_app.py
import sys
import os

# Добавляем текущий каталог в PYTHONPATH
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import pytest
from flask import url_for
from app import app  # Импорт вашего Flask приложения

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

def test_home(client):
    app.config['SERVER_NAME'] = 'localhost'
    app.config['PREFERRED_URL_SCHEME'] = 'http'
    with app.app_context():
        response = client.get(url_for('home'))
        assert response.status_code == 200
        assert b"<h1>" in response.data

def test_hello(client):
    app.config['SERVER_NAME'] = 'localhost'
    app.config['PREFERRED_URL_SCHEME'] = 'http'
    with app.app_context():
        response = client.get(url_for('hello'))
        assert response.status_code == 200
        assert b"<!DOCTYPE html>" in response.data
        assert b"Hello User!" in response.data
